import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
 * Test Driven Development (TDD)
 * 
 * @author Francisco Guerra
 */
@RunWith(GreenfootRunner.class)
public class WormTest {

    CrabWorld crabWorld;
    Worm worm;
    
    @Before
    public void setUp() throws Exception {
        // GIVEN the crab world and the first of the ten worms
        crabWorld = new CrabWorld();
        assertEquals(10, crabWorld.getObjects(Worm.class).size());
        worm = crabWorld.getObjects(Worm.class).get(0);
    }

    @Test
    public void testNoMove() throws Exception {
        // GIVEN the initial position of the worm
        int x = worm.getX();
        int y = worm.getY();
        
        for (int k=0; k<20; k++) {
            // WHEN time goes by
            worm.act();
            
            // THEN the worm is still in the same place
            assertEquals(x, worm.getX());
            assertEquals(y, worm.getY());
        }
    }

}
