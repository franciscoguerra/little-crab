import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import greenfoot.junitUtils.runner.GreenfootRunner;

/**
 * Test Driven Development (TDD)
 * 
 * @author Francisco Guerra
 */
@RunWith(GreenfootRunner.class)
public class CrabWorldTest {
    
    @Before
    public void setUp() throws Exception {
    }
    
    @Test
    public void testConstructor() throws Exception {
        // GIVEN
        CrabWorld crabWorld;
        
        // WHEN the crab world is created
        crabWorld = new CrabWorld();
        
        // THEN there are 1 crab, 3 lobsters and 10 worms in a world of 560x560x1
        assertEquals(560, crabWorld.getWidth());
        assertEquals(560, crabWorld.getHeight());
        assertEquals(  1, crabWorld.getCellSize());
        assertEquals(  1, crabWorld.getObjects(Crab.class).size());
        assertEquals(  3, crabWorld.getObjects(Lobster.class).size());
        assertEquals( 10, crabWorld.getObjects(Worm.class).size());
    }

}
