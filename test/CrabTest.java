import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import greenfoot.junitUtils.runner.GreenfootRunner;

import greenfoot.GreenfootImage;
import greenfoot.junitUtils.EventDispatch;
import greenfoot.junitUtils.SoundPlayed;
import greenfoot.junitUtils.WorldCreator;

/**
 * Test Driven Development (TDD)
 * 
 * @author Francisco Guerra
 */
@RunWith(GreenfootRunner.class)
public class CrabTest {

    CrabWorld crabWorld;
    Crab crab;
    
    @Before
    public void setUp() throws Exception {
        // GIVEN a crab world with its crab
        crabWorld = new CrabWorld();
        assertEquals(1, crabWorld.getObjects(Crab.class).size());
        crab = crabWorld.getObjects(Crab.class).get(0);
    }

    @Test
    public void testGetX() throws Exception {
        // WHEN nothing
        
        // THEN the crab is still in the same place
        assertEquals(231, crab.getX());
    }

    @Test
    public void testGetY() throws Exception {
        // WHEN nothing
        
        // THEN the crab is still in the same place
        assertEquals(203, crab.getY());
    }
    
    @Test
    public void testGetRotation() throws Exception {
        // WHEN nothing
        
        // THEN the crab is still in the same direction
        assertEquals(0, crab.getRotation());
    }
    
    @Test
    public void testGetImage() throws Exception {
        // WHEN nothing
        
        // THEN the crab has its initial image
        assertEquals("crab.png", crab.getImage().getImageFileName());
    }
    
    @Test
    public void testMove() throws Exception {
        // GIVEN the initial position
        int x = crab.getX();
        int y = crab.getY();
        
        // WHEN any key is not typed
        crab.act();
        
        // THEN the crab moves 5
        assertEquals(x+5, crab.getX());
        assertEquals(  y, crab.getY());
    }

    @Test
    public void testTurnRight() throws Exception {
        // GIVEN the initial position
        assertEquals(0, crab.getRotation());
        
        // WHEN right key is typed
        EventDispatch.keyTyped("right");
        crab.act();
        
        // THEN the crab rotates 4 degrees to the right
        assertEquals(4, crab.getRotation());
    }

    @Test
    public void testTurnRightBis() throws Exception {
        // GIVEN the initial position
        int degrees = crab.getRotation();
        
        // WHEN right key is typed
        EventDispatch.keyTyped("right");
        crab.act();
        
        // THEN the crab rotates 4 degrees to the right
        assertEquals((360+degrees+4)%360, crab.getRotation());
    }

    @Test
    public void testTurnRightBisBis() throws Exception {
        // GIVEN the initial position
        crab.setRotation(10);
        
        // WHEN right key is typed
        EventDispatch.keyTyped("right");
        crab.act();
        
        // THEN the crab rotates 4 degrees to the right
        assertEquals(14, crab.getRotation());
    }

    @Test
    public void testTurnLeft() throws Exception {
        // GIVEN the initial position
        assertEquals(0, crab.getRotation());
        
        // WHEN left key is typed
        EventDispatch.keyTyped("left");
        crab.act();
        
        // THEN the crab rotates 4 degrees to the left
        assertEquals(356, crab.getRotation());
    }

    @Test
    public void testTurnLeftBis() throws Exception {
        // GIVEN the initial position
        int degrees = crab.getRotation();
        
        // WHEN left key is typed
        EventDispatch.keyTyped("left");
        crab.act();
        
        // THEN the crab rotates 4 degrees to the left
        assertEquals((360+degrees-4)%360, crab.getRotation());
    }

    @Test
    public void testTurnLeftBisBis() throws Exception {
        // GIVEN the initial position
        crab.setRotation(10);
        
        // WHEN left key is typed
        EventDispatch.keyTyped("left");
        crab.act();
        
        // THEN the crab rotates 4 degrees to the left
        assertEquals(6, crab.getRotation());
    }

    @Test
    public void testTurnRight90() throws Exception {
        // GIVEN a crab that has rotated 90 degrees to the right
        for (int k=0; k<90/4;k++){
            EventDispatch.keyTyped("right");
            crab.act();
        }
        
        // WHEN any key is not typed
        int x = crab.getX();
        int y = crab.getY();
        crab.act();
        
        // THEN the crab goes down 5
        assertEquals(  x, crab.getX());
        assertEquals(y+5, crab.getY());
    }

    @Test
    public void testTurnRight180() throws Exception {
        // GIVEN a crab that has rotated 180 degrees to the right
        for (int k=0; k<180/4;k++){
            EventDispatch.keyTyped("right");
            crab.act();
        }
        
        // WHEN any key is not typed
        int x = crab.getX();
        int y = crab.getY();
        crab.act();
        
        // THEN the crab goes back 5
        assertEquals(x-5, crab.getX());
        assertEquals(  y, crab.getY());
    }

    @Test
    public void testTurnRight270() throws Exception {
        // GIVEN a crab that has rotated 270 degrees to the right
        for (int k=0; k<270/4;k++){
            EventDispatch.keyTyped("right");
            crab.act();
        }
        
        // WHEN any key is not typed
        int x = crab.getX();
        int y = crab.getY();
        crab.act();
        
        // THEN the crab goes up 5
        assertEquals(  x, crab.getX());
        assertEquals(y-5, crab.getY());
    }

    @Test
    public void testTurnLeft90() throws Exception {
        // GIVEN a crab that has rotated 90 degrees to the left
        for (int k=0; k<90/4;k++){
            EventDispatch.keyTyped("left");
            crab.act();
        }
        
        // WHEN any key is not typed
        int x = crab.getX();
        int y = crab.getY();
        crab.act();
        
        // THEN the crab goes up 5
        assertEquals(  x, crab.getX());
        assertEquals(y-5, crab.getY());
    }

    @Test
    public void testTurnLeft180() throws Exception {
        // GIVEN a crab that has rotated 180 degrees to the left
        for (int k=0; k<180/4;k++){
            EventDispatch.keyTyped("left");
            crab.act();
        }
        
        // WHEN any key is not typed
        int x = crab.getX();
        int y = crab.getY();
        crab.act();
        
        // THEN the crab goes back 5
        assertEquals(x-5, crab.getX());
        assertEquals(  y, crab.getY());
    }

    @Test
    public void testTurnLeft270() throws Exception {
        // GIVEN a crab that has rotated 270 degrees to the left
        for (int k=0; k<270/4;k++){
            EventDispatch.keyTyped("left");
            crab.act();
        }
        
        // WHEN any key is not typed
        int x = crab.getX();
        int y = crab.getY();
        crab.act();
        
        // THEN the crab goes down 5
        assertEquals(  x, crab.getX());
        assertEquals(y+5, crab.getY());
    }
    
    @Test
    public void testChangeImage() throws Exception {
        // GIVEN "crab.png" is the initial image of crab 
        assertEquals("crab.png", crab.getImage().getImageFileName());        

        // WHEN the crab travels the world
        crab.act();

        // THEN the crab changes its image
        assertEquals("crab2.png", crab.getImage().getImageFileName());        
    }
    
    @Test
    public void testChangeImageAgain() throws Exception {
        // GIVEN A crab that has changed its image
        crab.act();
        assertEquals("crab2.png", crab.getImage().getImageFileName());        
        
        // WHEN the crab travels the world
        crab.act();

        // THEN the crab recovers its initial image
        assertEquals("crab.png", crab.getImage().getImageFileName());        
    }
    
    @Test
    public void testSwitchImage() throws Exception {
        // GIVEN the two posible images
        String image[] = {"crab.png", "crab2.png"};
        assertEquals(image[0], crab.getImage().getImageFileName());

        for (int k=1;k<10;k++) {
            // WHEN the crab travels the world
            crab.act();

            // THEN the crab changes its image
            assertEquals(image[k%2], crab.getImage().getImageFileName());
        }
    }
    
    @Test
    public void testSwitchImageBis() throws Exception {
        // GIVEN the two posible images
        GreenfootImage image[] = {new GreenfootImage("crab.png"),
                                  new GreenfootImage("crab2.png")};
        assertEquals(image[0], crab.getImage());
                                  
        for (int k=1;k<10;k++) {
            // WHEN the crab travels the world
            crab.act();

            // THEN the crab changes its image
            assertEquals(image[k%2], crab.getImage());
        }
    }
    
    @Test
    public void testLookForWorm() throws Exception {
        // GIVEN a worm in the same position as the crab
        Worm worm = new Worm();
        int x = crab.getX();
        int y = crab.getY();
        crabWorld.addObject(worm, x, y);
        assertTrue(crabWorld.getObjectsAt(x, y, Worm.class).contains(worm));

        // WHEN the crab checks that there is a worm
        crab.act();
        
        // THEN you can listen to how the crab eats the worm
        assertEquals("slurp.wav", SoundPlayed.get());
        assertFalse(crabWorld.getObjectsAt(x, y, Worm.class).contains(worm));
    }

    @Test
    public void testLookForWormBis() throws Exception {
        // GIVEN a worm in the same position as the crab
        assertTrue(crabWorld.getObjects(Worm.class).size()>0);
        Worm worm = crabWorld.getObjects(Worm.class).get(0);
        crab.setLocation(worm.getX(), worm.getY());

        // WHEN the crab checks that there is a worm
        crab.act();
        
        // THEN you can listen to how the crab eats the worm
        assertEquals("slurp.wav", SoundPlayed.get());
        assertFalse(crabWorld.getObjectsAt(crab.getX(), crab.getY(), Worm.class).contains(worm));
    }

    @Test
    public void testLookForAllWorm() throws Exception {
        // GIVEN a crab that has eaten seven worms
        for (int k=0; k<7; k++) {
            int x = crab.getX();
            int y = crab.getY();
            Worm worm = new Worm();
            crabWorld.addObject(worm, x, y);
            assertTrue(crabWorld.getObjectsAt(x, y, Worm.class).contains(worm));
            crab.act();
            assertEquals("slurp.wav", SoundPlayed.get());
            assertFalse(crabWorld.getObjectsAt(x, y, Worm.class).contains(worm));
        }
        
        // WHEN the crab eats the eighth worm
        int x = crab.getX();
        int y = crab.getY();
        Worm worm = new Worm();
        crabWorld.addObject(worm, x, y);
        assertTrue(crabWorld.getObjectsAt(x, y, Worm.class).contains(worm));
        crab.act();
        assertEquals("slurp.wav", SoundPlayed.get());
        assertFalse(crabWorld.getObjectsAt(x, y, Worm.class).contains(worm));
        
        // THEN the trumpets are heard and the game over
        assertEquals("fanfare.wav", SoundPlayed.get());
        assertTrue(WorldCreator.isStop());
    }

    @Test
    public void testLookForAllWormBis() throws Exception {
        // GIVEN a crab that has eaten seven worms
        for (int k=0; k<7; k++) {
            Worm worm = new Worm();
            crabWorld.addObject(worm, crab.getX(), crab.getY());
            crab.act();
        }
        
        // WHEN the crab eats the eighth worm
        int x = crab.getX();
        int y = crab.getY();
        Worm worm = new Worm();
        crabWorld.addObject(worm, x, y);
        assertTrue(crabWorld.getObjectsAt(x, y, Worm.class).contains(worm));
        WorldCreator.runOnce(crab); // To discard the sounds played 
                                    // before eating the eighth worm
        assertEquals("slurp.wav", SoundPlayed.get());
        assertFalse(crabWorld.getObjectsAt(x, y, Worm.class).contains(worm));
        
        // THEN the trumpets are heard and the game over
        assertEquals("fanfare.wav", SoundPlayed.get());
        assertTrue(WorldCreator.isStop());
    }

    @Test
    public void testLookForAllWormBisBis() throws Exception {
        // GIVEN a crab that has eaten seven worms
        for (int k=0; k<7; k++) {
            Worm worm = new Worm();
            crabWorld.addObject(worm, crab.getX(), crab.getY());
            crab.act();
        }
        
        // WHEN the crab eats the eighth worm
        Worm worm = new Worm();
        crabWorld.addObject(worm, crab.getX(), crab.getY());
        crab.act();
        
        // THEN the trumpets are heard and the game over
        assertTrue(SoundPlayed.get("fanfare.wav"));
        assertTrue(WorldCreator.isStop());
    }

    @Test
    public void testLookForAllWormBisBisBis() throws Exception {
        // GIVEN a crab that has eaten seven worms
        for (int k=0; k<7; k++) {
            assertTrue(crabWorld.getObjects(Worm.class).size()>0);
            Worm worm = crabWorld.getObjects(Worm.class).get(0);
            crab.setLocation(worm.getX(), worm.getY());
            crab.act();
        }
        
        // WHEN the crab eats the eighth worm
        assertTrue(crabWorld.getObjects(Worm.class).size()>0);
        Worm worm = crabWorld.getObjects(Worm.class).get(0);
        crab.setLocation(worm.getX(), worm.getY());
        crab.act();
        
        // THEN the trumpets are heard and the game over
        assertTrue(SoundPlayed.get("fanfare.wav"));
        assertTrue(WorldCreator.isStop());
    }
}
