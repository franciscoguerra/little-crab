import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import greenfoot.junitUtils.runner.GreenfootRunner;

import greenfoot.junitUtils.Random;
import greenfoot.junitUtils.SoundPlayed;

/**
 * Test Driven Development (TDD)
 * 
 * @author Francisco Guerra
 */
@RunWith(GreenfootRunner.class)
public class LobsterTest {

    CrabWorld crabWorld;
    Lobster lobster;
    
    @Before
    public void setUp() throws Exception {
        // GIVEN the crab world and the first of the three lobsters
        crabWorld = new CrabWorld();
        assertEquals(3, crabWorld.getObjects(Lobster.class).size());
        lobster = crabWorld.getObjects(Lobster.class).get(0);
    }

    @Test
    public void testMove() throws Exception {
        // GIVEN the initial position of the first lobster
        int x = lobster.getX();
        int y = lobster.getY();
        
        // WHEN the random number is less than or equal to 90
        Random.set(90);
        lobster.act();
        
        // THEN the lobster does not turn and moves 5
        assertEquals(x+5, lobster.getX());
        assertEquals(  y, lobster.getY());
        assertEquals(  0, lobster.getRotation());
    }

    @Test
    public void testMoveAtEdge() throws Exception {
        // GIVEN the edge position of the first lobster
    	int x = lobster.getWorld().getWidth()-1;
    	int y = 100;
    	lobster.setLocation(x, y);
        
        
        // WHEN the random number is less than or equal to 90
        Random.set(90);
        lobster.act();
        
        // THEN the lobster moves 1 and turns 17
        assertEquals(  x, lobster.getX());
        assertEquals(y+1, lobster.getY());
        assertEquals( 17, lobster.getRotation());
    }
    
    @Test
    public void testTwoMoveAtEdge() throws Exception {
        // GIVEN the edge position of the first lobster
    	int x = lobster.getWorld().getWidth()-1;
    	int y = 100;
    	lobster.setLocation(x, y);
        
        
        // WHEN the random number is less than or equal to 90
        Random.set(90);
        lobster.act();
        Random.set(90);
        lobster.act();
        
        // THEN the lobster moves 4 and turns 17+17
        assertEquals(  x, lobster.getX());
        assertEquals(y+4, lobster.getY());
        assertEquals( 34, lobster.getRotation());
    }
    
    @Test
    public void testTurnRight() throws Exception {
        // GIVEN the initial position of the first lobster
        int x = lobster.getX();
        int y = lobster.getY();
        
        // WHEN the random number is greater than 90
        Random.set(91); // random number is greater than 90
        Random.set(90); // random number to turn 45 = 90 - 45
        lobster.act();
        
        // THEN the lobster turns 45 degrees and moves diagonally
        assertEquals(x+4, lobster.getX());
        assertEquals(y+4, lobster.getY());
        assertEquals( 45, lobster.getRotation());
    }

    @Test
    public void testTurnLeft() throws Exception {
        // GIVEN the initial position of the first lobster
        int x = lobster.getX();
        int y = lobster.getY();
        
        // WHEN the random number is greater than 90
        Random.set(91); // random number is greater than 90
        Random.set(0);  // random number to turn -45 = 0 - 45
        lobster.act();
        
        // THEN the lobster turns -45 degrees and moves diagonally
        assertEquals(x+4, lobster.getX());
        assertEquals(y-4, lobster.getY());
        assertEquals(315, lobster.getRotation());
    }

    @Test
    public void testTurnRightAtEdge() throws Exception {
        // GIVEN the edge position of the first lobster
    	int x = lobster.getWorld().getWidth()-1;
    	int y = 150;
    	lobster.setLocation(x, y);
        
        
        // WHEN the random number is greater than 90
        Random.set(91); // random number is greater than 90
        Random.set(90); // random number to turn 45 = 90 - 45
        lobster.act();
        
        // THEN the lobster moves 4 turns 62
        assertEquals(    x, lobster.getX());
        assertEquals(  y+4, lobster.getY());
        assertEquals(45+17, lobster.getRotation());
    }
    
    @Test
    public void testTurnLeftAtEdge() throws Exception {
        // GIVEN the edge position of the first lobster
    	int x = lobster.getWorld().getWidth()-1;
    	int y = 150;
    	lobster.setLocation(x, y);
        
        
        // WHEN the random number is greater than 90
        Random.set(91); // random number is greater than 90
        Random.set(0);  // random number to turn -45 = 0 - 45
        lobster.act();
        
        // THEN the lobster moves -2 and turns -28
        assertEquals(        x, lobster.getX());
        assertEquals(      y-2, lobster.getY());
        assertEquals(360+17-45, lobster.getRotation());
    }
    
    @Test
    public void testTurnRight90() throws Exception {
        // GIVEN a lobster that has rotated 90 to the right
        for (int k=0; k<2;k++){
            Random.set(91); // random number to turn
            Random.set(90); // random number to turn 45 = 90 - 45
            lobster.act();
        }
        int x = lobster.getX();
        int y = lobster.getY();
        
        // WHEN the lobster does not turn again
        Random.set(0);
        lobster.act();
        
        // THEN the lobster goes down 5
        assertEquals(  x, lobster.getX());
        assertEquals(y+5, lobster.getY());
    }

    @Test
    public void testTurnRight180() throws Exception {
        // GIVEN a lobster that has rotated 180 degrees to the right
        for (int k=0; k<4;k++){
            Random.set(91); // random number to turn
            Random.set(90); // random number to turn 45 = 90 - 45
            lobster.act();
        }
        int x = lobster.getX();
        int y = lobster.getY();
        
        // WHEN the lobster does not turn again
        Random.set(0);
        lobster.act();
        
        // THEN the lobster goes back 5
        assertEquals(x-5, lobster.getX());
        assertEquals(  y, lobster.getY());
    }

    @Test
    public void testTurnRight270() throws Exception {
        // GIVEN a lobster that has rotated 270 degrees to the right
        for (int k=0; k<6;k++){
            Random.set(91); // random number to turn
            Random.set(90); // random number to turn 45 = 90 - 45
            lobster.act();
        }
        int x = lobster.getX();
        int y = lobster.getY();
        
        // WHEN the lobster does not turn again
        Random.set(0);
        lobster.act();
        
        // THEN the lobster goes up 5
        assertEquals(  x, lobster.getX());
        assertEquals(y-5, lobster.getY());
    }
    
    @Test
    public void testTurnLeft90() throws Exception {
        // GIVEN a lobster that has rotated 90 to the left
        for (int k=0; k<2;k++){
            Random.set(91); // random number to turn
            Random.set(0);  // random number to turn -45 = 0 - 45
            lobster.act();
        }
        int x = lobster.getX();
        int y = lobster.getY();
        
        // WHEN the lobster does not turn again
        Random.set(0);
        lobster.act();
        
        // THEN the lobster goes up 5
        assertEquals(  x, lobster.getX());
        assertEquals(y-5, lobster.getY());
    }

    @Test
    public void testTurnLeft180() throws Exception {
        // GIVEN a lobster that has rotated 180 degrees to the left
        for (int k=0; k<4;k++){
            Random.set(91); // random number to turn
            Random.set(0);  // random number to turn -45 = 0 - 45
            lobster.act();
        }
        int x = lobster.getX();
        int y = lobster.getY();
        
        // WHEN the lobster does not turn again
        Random.set(0);
        lobster.act();
        
        // THEN the lobster goes back 5
        assertEquals(x-5, lobster.getX());
        assertEquals(  y, lobster.getY());
    }

    @Test
    public void testTurnLeft270() throws Exception {
        // GIVEN a lobster that has rotated 270 degrees to the left
        for (int k=0; k<6;k++){
            Random.set(91); // random number to turn
            Random.set(0);  // random number to turn -45 = 0 - 45
            lobster.act();
        }
        int x = lobster.getX();
        int y = lobster.getY();
        
        // WHEN the lobster does not turn again
        Random.set(0);
        lobster.act();
        
        // THEN the lobster goes down 5
        assertEquals(  x, lobster.getX());
        assertEquals(y+5, lobster.getY());
    }
        
    @Test
    public void testLookForCrab() throws Exception {
        // GIVEN a crab in the same position as the lobster
        Crab crab = new Crab();
        int x = lobster.getX();
        int y = lobster.getY();
        crabWorld.addObject(crab, x, y);
        assertTrue(crabWorld.getObjectsAt(x, y, Crab.class).contains(crab));
        
        // WHEN the lobster checks that there is a crab
        lobster.act();
        
        // THEN the crab's complaint can be listened when the lobster eats it
        assertEquals("au.wav", SoundPlayed.get());
        assertFalse(crabWorld.getObjectsAt(x, y, Crab.class).contains(crab));
    }

    @Test
    public void testLookForCrabBis() throws Exception {
        // GIVEN a crab in the same position as the lobster
        assertTrue(crabWorld.getObjects(Crab.class).size()>0);
        Crab crab = crabWorld.getObjects(Crab.class).get(0);
        lobster.setLocation(crab.getX(), crab.getY());
        
        // WHEN the lobster checks that there is a crab
        lobster.act();
        
        // THEN the crab's complaint can be listened when the lobster eats it
        assertEquals("au.wav", SoundPlayed.get());
        assertFalse(crabWorld.getObjectsAt(lobster.getX(), lobster.getY(), Crab.class).contains(crab));
    }
}
